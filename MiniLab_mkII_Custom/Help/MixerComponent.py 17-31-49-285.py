from __future__ import absolute_import, print_function, unicode_literals
from .ArturiaMixerComponent import MixerComponent as MixerComponentBase

class MixerComponent(MixerComponentBase):

    def __init__(self, *a, **k):
        super(MixerComponent, self).__init__(*a, **k)

    
