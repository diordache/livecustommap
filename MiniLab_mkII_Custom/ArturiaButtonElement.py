from __future__ import absolute_import, print_function, unicode_literals
from _Framework.ButtonElement import ButtonElement as ButtonElementBase

EMPTY_VALUE = 0
RECORDING_VALUE = 1
STOP_BUTTON_VALUE = 1
MUTE_BUTTON_VALUE = 5
SOLO_BUTTON_VALUE = 4

class ButtonElement(ButtonElementBase):
    _color_on = RECORDING_VALUE
    _color_off = EMPTY_VALUE
    _is_on = False

    def __init__(self, is_momentary, msg_type, channel, identifier, *a, **k):
        super(ButtonElement, self).__init__(is_momentary, msg_type, channel, identifier, *a, **k)
        self._led = None
        return

    def turn_on(self):
        if self._led:
            self._led.send_value((self._color_on,))
            self._is_on = True
        
    def turn_off(self):
        if self._led:
            self._led.send_value((self._color_off,))
            self._is_on = False

    def set_led(self, led):
        self._led = led

    #0 is rec
    #1 is solo
    #2 is stop
    #3 is mute
    def set_color(self, color):
        if color == 0:
            self._color_on = RECORDING_VALUE
            self._color_off = EMPTY_VALUE
        if color == 2:
            self._color_on = STOP_BUTTON_VALUE
            self._color_off = STOP_BUTTON_VALUE
        if color == 3:
            self._color_on = EMPTY_VALUE
            self._color_off = MUTE_BUTTON_VALUE
        if color == 1:
            self._color_on = SOLO_BUTTON_VALUE
            self._color_off = EMPTY_VALUE

        if self._is_on:
            self.turn_on()
        else:
            self.turn_off()

  