from __future__ import absolute_import, print_function, unicode_literals
from _Framework.Control import EncoderControl
from _Framework.ScrollComponent import ScrollComponent as ScrollComponentBase

class SelectorComponent(ScrollComponentBase):
	selector_encoder = EncoderControl()
	cb = None

	def set_selector_encoder(self, encoder):
		self.selector_encoder.set_control_element(encoder)
		self.update()

	def set_cb(self, cb):
		self.cb = cb

	@selector_encoder.value
	def selector_encoder(self, value, encoder):
		self.cb(True)
		return
