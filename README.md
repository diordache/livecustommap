# Arturia Minilab Mk II - Ableton Live Custom map #

This is a custom map for integration with Ableton Live.
It uses the Pads 9-16 to control the rec,mute,stop,solo for 8 tracks in Live.

## Features ##

1. Control more function from the Minilab Mk2, like Rec, Solo, Mute, Stop Track
1. Light the pads depending on what function is done at a certain moment

## Instalation ##

Download by using the `Downloads` button on the left and then click on `Download Repository`

Extract, you will need the `Minilab_MkII_Custom` folder from the extracted archive. 

### Mac OS ###

Copy the `Minilab_MkII_Custom` folder to
`{INSTALL_PATH}/Contents/App-Resources/MIDI Remote Scripts`

For me INSTALL_PATH is `/Applications/Ableton Live 10 Lite.app/`

There is already a folder named `Minilab_MkII`, `Minilab_MkII_Custom` should be copied next to it.

If `Live` is running, close the application and restart it.

### Windows ###

It should be similar to Mac OS, probably Live is in `C:\Program Files\Ableton Live 10 Lite`

## Usage ##

1. Open Live
1. Select Minilab Mk2 Custom in `Preferences`>`Link Midi` as a Control Surface
1. Put the Minilab Mk2 in Live mode by `SHIFT`+`Pad 8`

### Control ###

1. Press `SHIFT` to light the Pads for 9-16 functions
1. The pads will control the Rec option for the tracks - if Rec is enabled for a track then the pad will light in Red
1. By pressing on the `Knob 9` will cycle through: solo,mute,stop for the first 8 tracks
1. Colors will change to indicate what is currently controlled
    1. All pads Red - Stop function for track
    1. All pads Yellow - Control Mute function, Muted tracks will have the light off
    1. All pads Off - Control Solo, Solo track will light is Green
1. To return to controlling the Rec press on the `Knob 1`

## Testing ##

* Live 10 Lite
* If you test with a different version, message me with version and will write here

## License ##
"THE BEER-WARE LICENSE"
If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.